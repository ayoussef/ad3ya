package devology.ad3ya.dao;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import devology.ad3ya.model.Article;
import devology.ad3ya.model.Category;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;


public class DataDao extends SQLiteOpenHelper{

	 //The Android's default system path of your application database.
    private static String DB_PATH = "/data/data/devology.ad3ya/databases/";
 
    private SQLiteDatabase myDataBase; 
 
    private final Context myContext;

    private static final int DATABASE_VERSION = 1;
    private static Boolean DEBUG_MODE = true;
 
    // Database Name
    private static final String DATABASE_NAME = "rasayleid";
	
	public DataDao(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		this.myContext = context;
	}
	
	
	/**
     * Creates a empty database on the system and rewrites it with your own database.
     * */
    public void createDataBase(Boolean testing) throws IOException{
    	DEBUG_MODE = testing;
    	boolean dbExist = checkDataBase();
 
    	if(dbExist){
    		//do nothing - database already exist
    	}else{
 
    		//By calling this method and empty database will be created into the default system path
               //of your application so we are gonna be able to overwrite that database with our database.
        	this.getReadableDatabase();
  
        	try {
 
    			copyDataBase();
 
    		} catch (IOException e) {
 
        		throw new Error("Error copying database");
 
        	}
    	}
 
    }
 
    /**
     * Check if the database already exist to avoid re-copying the file each time you open the application.
     * @return true if it exists, false if it doesn't
     */
    private boolean checkDataBase(){
 
    	if(!DEBUG_MODE) {
	    	SQLiteDatabase checkDB = null;
	 
	    	try{
	    		String myPath = DB_PATH + DATABASE_NAME;
	    		checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
	 
	    	}catch(SQLiteException e){
	 
	    		//database does't exist yet.
	 
	    	}
	 
	    	if(checkDB != null){
	 
	    		checkDB.close();
	 
	    	}
	 
	    	return checkDB != null ? true : false;
    	}else {
    		return false;
    	}
    }
 
    /**
     * Copies your database from your local assets-folder to the just created empty database in the
     * system folder, from where it can be accessed and handled.
     * This is done by transfering bytestream.
     * */
    private void copyDataBase() throws IOException{
 
    	//Open your local db as the input stream
    	InputStream myInput = myContext.getAssets().open(DATABASE_NAME);
 
    	// Path to the just created empty db
    	String outFileName = DB_PATH + DATABASE_NAME;
 
    	//Open the empty db as the output stream
    	OutputStream myOutput = new FileOutputStream(outFileName);
 
    	//transfer bytes from the inputfile to the outputfile
    	byte[] buffer = new byte[1024];
    	int length;
    	while ((length = myInput.read(buffer))>0){
    		myOutput.write(buffer, 0, length);
    	}
 
    	//Close the streams
    	myOutput.flush();
    	myOutput.close();
    	myInput.close();
 
    }
 
    public void openDataBase() throws SQLException{
 
    	//Open the database
        String myPath = DB_PATH + DATABASE_NAME;
    	myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
 
    }
 
    @Override
	public synchronized void close() {
    	    if(myDataBase != null)
    		    myDataBase.close();
    	    super.close();
	}
    
    @Override
	public void onCreate(SQLiteDatabase db) {
//		String CREATE_CONTACTS_TABLE = "CREATE TABLE category (_id integer primary key, name text, desc text, parentId numeric)";
//        db.execSQL(CREATE_CONTACTS_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
	}

	
    
///////////////////////////////////////////////////////////////////
//// Utility methods start here
/////////////////////////////////////////////////////////////////
 
 
	public void addCategory(Category category) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
        values.put("name", category.getName()); 
        values.put("desc", category.getDesc()); 
        values.put("parentId", category.getParentId());
        db.insert("category", null, values);
        db.close();
	}
	
	public Category getCategory(int id) {
		SQLiteDatabase db = this.getReadableDatabase();
		 
        Cursor cursor = db.query("category", new String[] { "_id",
                "name", "desc", "parentId" }, "_id" + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
 
        Category category = new Category();
        category.setId(Integer.parseInt(cursor.getString(0)));
        category.setName(String.valueOf(cursor.getString(1)));
        category.setDesc(String.valueOf(cursor.getString(2)));
        if(cursor.getString(3) != null) {
        	category.setParentId(Integer.parseInt(cursor.getString(3)));
        } 
        return category;
	}


	public List<Category> getCategories(Integer parentId) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor;
		if(parentId != null) {
			cursor = db.query("category", new String[] { "_id",
	                "name", "desc", "parentId" },"parentId=?",
	                new String[] { String.valueOf(parentId) }, null, null, null, null);
		}else {
			cursor = db.query("category", new String[] { "_id",
	                "name", "desc", "parentId" },"parentId is null",null, null, null, null, null);
		}
		List<Category> categories = new ArrayList<Category>();
		if (cursor.moveToFirst()) {
	        do {
	            Category category = new Category();
	            category.setId(Integer.parseInt(cursor.getString(0)));
	            category.setName(cursor.getString(1));
	            category.setDesc(cursor.getString(2));
	            if(cursor.getString(3) != null) {
	            	category.setParentId(Integer.parseInt(cursor.getString(3)));
	            }
	            categories.add(category);
	        } while (cursor.moveToNext());
	    }
		return categories;
	}


	public List<Article> getArticlesByParentId(Integer parentId) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query("article", new String[] { "_id",
                "name", "desc", "parentId", "faved" },"parentId=?" ,
                new String[] { String.valueOf(parentId) }, null, null, null, null);
		List<Article> articles = new ArrayList<Article>();
		if (cursor.moveToFirst()) {
	        do {
	            Article article = new Article();
	            article.setId(Integer.parseInt(cursor.getString(0)));
	            article.setName(cursor.getString(1));
	            article.setDesc(cursor.getString(2));
	            if(cursor.getString(3) != null) {
	            	article.setParentId(Integer.parseInt(cursor.getString(3)));
	            }
	            if(cursor.getString(4) != null) {
	            	article.setFaved(Integer.valueOf(cursor.getString(4)));
	            }
	            articles.add(article);
	        } while (cursor.moveToNext());
	    }
		return articles;
	}


	public void saveArticle(Article article) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
        values.put("faved", article.getFaved());
        db.update("article", values, "_id="+article.getId(), null);
        db.close();
	}


	public List<Article> getFavouritArticles() {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query("article", new String[] { "_id",
                "name", "desc", "parentId", "faved" },"faved=?" ,
                new String[] { String.valueOf(1) }, null, null, null, null);
		List<Article> articles = new ArrayList<Article>();
		if (cursor.moveToFirst()) {
	        do {
	            Article article = new Article();
	            article.setId(Integer.parseInt(cursor.getString(0)));
	            article.setName(cursor.getString(1));
	            article.setDesc(cursor.getString(2));
	            if(cursor.getString(3) != null) {
	            	article.setParentId(Integer.parseInt(cursor.getString(3)));
	            }
	            if(cursor.getString(4) != null) {
	            	article.setFaved(Integer.valueOf(cursor.getString(4)));
	            }
	            articles.add(article);
	        } while (cursor.moveToNext());
	    }
		return articles;
	}


	public Integer getRated() {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query("user", new String[] { "rated"},"" ,
                new String[] {}, null, null, null, null);
		if (cursor.moveToFirst()) {
	        do {
	            return Integer.parseInt(cursor.getString(0));
	        } while (cursor.moveToNext());
	    }
		return 1;
	}


	public void updateRated(int rated) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
        values.put("rated", rated);
        db.update("user", values, "", null);
        db.close();		
	}

}
