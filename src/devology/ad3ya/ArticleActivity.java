package devology.ad3ya;

import java.util.Collections;
import java.util.List;
import java.util.Random;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.viewpagerindicator.TitlePageIndicator;

import devology.ad3ya.adapters.ViewPagerAdapter;
import devology.ad3ya.model.Article;
import devology.ad3ya.model.Category;
import devology.ad3ya.service.DataService;

public class ArticleActivity extends Activity{

	private List<Article> articles;
	static ViewPagerAdapter adapter;
    TextView articleBody;
    Animation animScale;
    
	@Override
    public void onCreate(Bundle savedInstanceState) {
		animScale = AnimationUtils.loadAnimation(this, R.layout.anim_scale);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article);
        final AdView adView = (AdView) findViewById(R.id.articleAdView);
        (new Thread() {
            public void run() {
                 Looper.prepare();
                 adView.loadAd(new AdRequest());
            }
        }).start();
        DataService dataService = DataService.getInstance(this);
        Intent intent = getIntent();
        Integer parentId = Integer.valueOf(intent.getStringExtra("parentId"));
        TextView categoryNameTextView = (TextView) findViewById(R.id.categoryName);
        if(parentId == 0) {
        	articles = dataService.getFavouritArticles();
        	categoryNameTextView.setText(getResources().getString(R.string.favourite_title));
        }else {
        	articles = dataService.getArticlesByParentId(parentId);
        	randomiseArticles(articles);
            Category category = dataService.getCategory(parentId);
            categoryNameTextView.setText(category.getName());	
        }
        adapter = new ViewPagerAdapter( this );
        if(articles != null && articles.size() > 0) {
        	adapter.setArticles(articles);
        	ViewPager pager = (ViewPager)findViewById( R.id.viewpager );
        	TitlePageIndicator indicator = 
        			(TitlePageIndicator)findViewById( R.id.indicator );
        	pager.setAdapter( adapter );
        	indicator.setViewPager( pager );
        	indicator.setFooterColor(0);
        }
        else {
        	AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        	alertDialog.setMessage(getResources().getString(R.string.favourite_empty));
        	alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Back", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                finish();
                }
        	});
        	alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
				public void onCancel(DialogInterface dialog) {
					finish();
				}
			});
        	alertDialog.show();
        }
    }
	
	
	private void randomiseArticles(List<Article> articles) {
		if(articles != null && articles.size() > 0) {
			long seed = System.nanoTime();
			Collections.shuffle(articles, new Random(seed));
		}
	}

	public void sendMessage(View view) {
		view.startAnimation(animScale);
		Intent smsIntent = new Intent(Intent.ACTION_VIEW);
		smsIntent.setType("vnd.android-dir/mms-sms");
		smsIntent.putExtra("sms_body",ViewPagerAdapter.articleBody);
		startActivity(smsIntent);
		launchMarket();
	}
    
	public void shareMessage(View view) {
		//create the send intent  
		Intent shareIntent =   
		 new Intent(android.content.Intent.ACTION_SEND);  
		  
		//set the type  
		shareIntent.setType("text/plain");  
		  
		//add a subject  
		shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,   
		 "app_name");  
		  
		//build the body of the message to be shared  
		String shareMessage = ViewPagerAdapter.articleBody;  
		  
		//add the message  
		shareIntent.putExtra(android.content.Intent.EXTRA_TEXT,   
		 shareMessage);  
		  
		//start the chooser for sharing  
		startActivity(Intent.createChooser(shareIntent, getResources().getString(R.string.choose_share_type)));
		launchMarket();
	}
	
	@TargetApi(11)
	public void copyMessage(View view) {
		view.startAnimation(animScale);
		int currentapiVersion = android.os.Build.VERSION.SDK_INT;
		if (currentapiVersion >= android.os.Build.VERSION_CODES.HONEYCOMB){
		     android.content.ClipboardManager clipboard =  (android.content.ClipboardManager) getSystemService(CLIPBOARD_SERVICE); 
		        ClipData clip = ClipData.newPlainText("label", ViewPagerAdapter.articleBody);
		        clipboard.setPrimaryClip(clip); 
		} else{
		    android.text.ClipboardManager clipboard = (android.text.ClipboardManager)getSystemService(CLIPBOARD_SERVICE); 
		    clipboard.setText(ViewPagerAdapter.articleBody);
		}
		
		Toast.makeText(this, R.string.text_copied, Toast.LENGTH_SHORT).show();
	}
	
	public void addMessageToFav(View view) {
//		view.startAnimation(animScale);
		Article article = adapter.getSelectedItem();
		article.setFaved(article.getFaved() * -1);
		DataService dataService = DataService.getInstance(this);
		dataService.saveArticle(article);
		Button button = (Button) findViewById(R.id.favButton);
		if(article.getFaved() == 1) {
			button.setText(R.string.removeFav_message);
        	button.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.insidemenu_unfav, 0, 0);
			Toast.makeText(this, R.string.saved_to_fav, Toast.LENGTH_SHORT).show();
		}else {
			button.setText(R.string.addFav_message);
        	button.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.insidemenu_fav, 0, 0);
			Toast.makeText(this, R.string.removed_from_fav, Toast.LENGTH_SHORT).show();
		}
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_article, menu);
        return true;
    }

    private void launchMarket() {
    	DataService dataService = DataService.getInstance(this);
    	Integer rated = dataService.getRated();
    	if(rated == 2) {
    		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        	alertDialog.setMessage(getResources().getString(R.string.rate_message));
        	alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getResources().getString(R.string.rate_ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                	Uri uri = Uri.parse("market://details?id=" + getPackageName());
        		    Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        		    try {
        		        startActivity(goToMarket);
        		    } catch (ActivityNotFoundException e) {
        		    }
                }
        	});
        	alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getResources().getString(R.string.rate_no), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                	DataService dataService = DataService.getInstance(new SplashScreenActivity());
                	dataService.updateRated(3);
                }
        	});
        	alertDialog.show();
    	}
    	if(rated <= 2) {
    		dataService.updateRated(rated);
    	}
	}

	public List<Article> getArticles() {
		return articles;
	}

	@Override
    public boolean onPrepareOptionsMenu (Menu menu) {
        return false;
    }

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}
    
}
