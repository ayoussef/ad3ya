package devology.ad3ya.adapters;

import java.util.List;

import devology.ad3ya.model.Category;
import devology.ad3ya.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class CategoryAdapter extends ArrayAdapter<Category>{
	
	private Context context = null;
	private List<Category> categories = null;
	
	public CategoryAdapter(Context context, int resource,
			int textViewResourceId, List<Category> objects) {
		super(context, resource, textViewResourceId, objects);
		this.context = context;
		this.categories = objects;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.rowlayout, parent, false);
	    rowView = inflater.inflate(R.layout.rowlayout, parent, false);
	    TextView textView = (TextView) rowView.findViewById(R.id.label);
	    textView.setText(categories.get(position).getName());
	    if(position % 2 == 0) {
	    	rowView.setBackgroundResource(R.drawable.grid_bg_1);
	    }
		return rowView;
	}

}
