package devology.ad3ya.adapters;
 
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import devology.ad3ya.ArticleActivity;
import devology.ad3ya.R;
import devology.ad3ya.model.Article;
 
public class ViewPagerAdapter extends PagerAdapter {
    private static List<Article> articles = new ArrayList<Article>();
    private final Context context;
    public static String articleBody;
    private Article selectedItem;
     
    public ViewPagerAdapter( Context context )
    {
        this.context = context;
    }
    
    @Override
    public int getCount()
    {
        return getArticles().size();
    }
 
    @Override
    public Object instantiateItem( View pager, int position )
    {
        TextView v = new TextView(context);
        v.setText( getArticles().get(position).getName() );
        v.setTextSize(17);
        v.setMovementMethod(new ScrollingMovementMethod());
        ((ViewPager)pager).addView( v, 0 );
        return v;
    }
    
    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
    	selectedItem = getArticles().get(position);
        Button button = (Button) ((ArticleActivity) context).findViewById(R.id.favButton);
        if(selectedItem.getFaved() == 1) {
        	button.setText(R.string.removeFav_message);
        	button.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.insidemenu_unfav, 0, 0);
        }else {
        	button.setText(R.string.addFav_message);
        	button.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.insidemenu_fav, 0, 0);
        }
    	articleBody = getArticles().get(position).getName();
    	super.setPrimaryItem(container, position, object);
    }
 
    @Override
    public void destroyItem( View pager, int position, Object view )
    {
        ((ViewPager)pager).removeView( (TextView)view );
    }
 
    @Override
    public boolean isViewFromObject( View view, Object object )
    {
        return view.equals( object );
    }
 
    @Override
    public void finishUpdate( View view ) {}
 
 
    @Override
    public void restoreState( Parcelable p, ClassLoader c ) {}
 
    @Override
    public Parcelable saveState() { 
        return null;
    }
 
    @Override
    public void startUpdate( View view ) {}

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}

	public Article getSelectedItem() {
		return selectedItem;
	}

}