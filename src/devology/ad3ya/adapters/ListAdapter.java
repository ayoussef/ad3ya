package devology.ad3ya.adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import devology.ad3ya.R;
import devology.ad3ya.model.Article;

public class ListAdapter extends ArrayAdapter<Article>{

	private Context context = null;
	private List<Article> articles = null;
	
	public ListAdapter(Context context, int resource, int textViewResourceId,
			List<Article> objects) {
		super(context, resource, textViewResourceId, objects);
		this.context = context;
		this.articles = objects;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
		        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		    View rowView = inflater.inflate(R.layout.rowlayout, parent, false);
		    TextView textView = (TextView) rowView.findViewById(R.id.label);
		    textView.setText(articles.get(position).getName());
		    textView.setPadding(10, 0, 10, 0);
		    return rowView;
	}

}
