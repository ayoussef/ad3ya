package devology.ad3ya;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import android.app.Activity;
import android.os.Bundle;
import android.os.Looper;
import android.view.Menu;
import android.view.Window;
import android.webkit.WebView;
import devology.ad3ya.tools.UserEmailFetcher;

public class AboutActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
    	requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        final String email = UserEmailFetcher.getEmail(this);
        (new Thread() {
            public void run() {
            	Looper.prepare();
        			URLConnection urlConnection;
					try {
						URL url = new URL("http://ahmedyoussef.org/android/removeNotification.php?appName=" + 
								getResources().getString(R.string.app_id) + "&email=" + email);
						urlConnection = url.openConnection();
						new InputStreamReader(urlConnection.getInputStream());
					} catch (IOException e) {
					}
            }
        }).start();
        WebView webView = (WebView) findViewById(R.id.aboutWebView);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.loadUrl("http://ahmedyoussef.org/android/about.html");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_about, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu (Menu menu) {
        return false;
    }

}
