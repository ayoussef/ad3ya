package devology.ad3ya;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;
import devology.ad3ya.adapters.ListAdapter;
import devology.ad3ya.model.Article;
import devology.ad3ya.service.DataService;

public class ArticleListActivity extends Activity {

	List<Article> articles;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_list);
        DataService dataService = DataService.getInstance(this);
        Intent intent = getIntent();
        Integer parentId = Integer.valueOf(intent.getStringExtra("parentId"));
        articles = dataService.getArticlesByParentId(parentId);
        ListAdapter listAdapter = new ListAdapter(this, android.R.layout.simple_list_item_1, android.R.id.text1,
        		articles);
        ListView listView = (ListView) findViewById(R.id.articles);
        listView.setAdapter(listAdapter);
        listView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				Toast.makeText(getApplicationContext(),
		        	      "Click ListItem Number " + position, Toast.LENGTH_LONG)
		        	      .show();
				goToArticleView(Integer.valueOf(String.valueOf(id)));
			}
        	}); 
    }
    
    public void goToArticleView(Integer id) {
    	Intent intent = new Intent(this, ArticleActivity.class);
    	intent.putExtra("parentId", String.valueOf(articles.get(id).getId()));
    	startActivity(intent);
	}

    @Override
    public boolean onPrepareOptionsMenu (Menu menu) {
        return false;
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_article_list, menu);
        return true;
    }
}
