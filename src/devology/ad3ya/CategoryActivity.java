package devology.ad3ya;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.google.ads.AdRequest;
import com.google.ads.AdView;

import devology.ad3ya.adapters.CategoryAdapter;
import devology.ad3ya.model.Category;
import devology.ad3ya.service.DataService;

public class CategoryActivity extends Activity {

	List<Category> categories;
	private static Integer parentId = null;
	private DataService dataService;
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        final AdView adView = (AdView) findViewById(R.id.categoryAdView);
        (new Thread() {
            public void run() {
                 Looper.prepare();
                 adView.loadAd(new AdRequest());
            }
        }).start();
        dataService = DataService.getInstance(this);
        Intent intent = getIntent();
        if(intent.getStringExtra("parentId") != null) {
        	parentId = Integer.valueOf(intent.getStringExtra("parentId"));
        }else {
        	parentId = null;
        }
        categories = dataService.getCategories(parentId);
        
        CategoryAdapter categoryAdapter = new CategoryAdapter(this, 
        		android.R.layout.simple_list_item_1, android.R.id.text1, categories);
        ListView listView = (ListView) findViewById(R.id.categories);
        listView.setAdapter(categoryAdapter);
        listView.setDivider(null);
        listView.setDividerHeight(0);
        listView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				parentId = categories.get((int)id).getId();
				goToNextListView(Integer.valueOf(String.valueOf(id)));
			}
        	}); 
    }
    
    public void goToNextListView(Integer id) {
    	Intent intent;
		List<Category> list = dataService.getCategories(parentId);
		if(list.size() > 0) {
			intent = new Intent(this, CategoryActivity.class);
		}else {
			intent = new Intent(this, ArticleActivity.class);	
		}
    	
    	intent.putExtra("parentId", String.valueOf(categories.get(id).getId()));
    	startActivity(intent);
    }
    
    @Override
    public boolean onPrepareOptionsMenu (Menu menu) {
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_category, menu);
        return true;
    }
    
}
