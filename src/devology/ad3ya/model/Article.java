package devology.ad3ya.model;

public class Article {

	private Integer id;
	
	private String name;
	
	private String desc;
	
	private Integer parentId;
	
	private Integer faved;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public Integer getFaved() {
		return faved;
	}

	public void setFaved(Integer faved) {
		this.faved = faved;
	}
	
	
}
