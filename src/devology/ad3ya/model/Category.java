package devology.ad3ya.model;


//@DatabaseTable(tableName = "category")
public class Category {
	
	private Integer id;
	
	private String name;
	
	private String desc;
	
	private Integer parentId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
	
	public String toString() {
		return getName();
	}
	
	
}
