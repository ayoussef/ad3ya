package devology.ad3ya;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import com.appbrain.AppBrain;
import com.google.ads.AdRequest;
import com.google.ads.AdView;

import devology.ad3ya.capricorn.RayLayout;
import devology.ad3ya.capricorn.RayMenu;
import devology.ad3ya.service.DataService;
import devology.ad3ya.tools.UserEmailFetcher;

public class SplashScreenActivity extends Activity {
	
	private final static Boolean TESTING = false;
	private static final int[] ITEM_DRAWABLES = { R.drawable.messages, R.drawable.share, R.drawable.fav, R.drawable.info};
	public static RayMenu menu;
	public static SplashScreenActivity activity;
	public SplashScreenActivity() {
		super();
	}
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	requestWindowFeature(Window.FEATURE_NO_TITLE); 
        super.onCreate(savedInstanceState);
        AppBrain.initApp(this);
        setContentView(R.layout.activity_splash_screen);
        activity = this;
        final AdView adView = (AdView) findViewById(R.id.articleAdView);
        (new Thread() {
            public void run() {
                 Looper.prepare();
                 adView.loadAd(new AdRequest());
            }
        }).start();
        DataService dataService = DataService.getInstance(this);
        try {
			dataService.createDatabase(TESTING);
		} catch (IOException e) {
			e.printStackTrace();
		}
        menu = (RayMenu) findViewById(R.id.ray_menu);

        final int itemCount = ITEM_DRAWABLES.length;
        for (int i = 0; i < itemCount; i++) {
            ImageView item = new ImageView(this);
            item.setImageResource(ITEM_DRAWABLES[i]);

            final int position = i;
            View.OnClickListener listner = new View.OnClickListener(){
                public void onClick(View v) {
                	switch (position) {
					case 0:
						goToCategories();
						break;
					case 1:
						shareApp();
						break;
					case 2:
						goToFavouriteList();
						break;
					case 3:
						goToAbout();
						break;
					default:
						break;
					}
                }
            };
            menu.addItem(item, listner );// Add a menu item
        }
        
        
        final String email = UserEmailFetcher.getEmail(this);
        (new Thread() {
            public void run() {
            	Looper.prepare();
            	try {
            		
                    URL url = new URL("http://ahmedyoussef.org/android/addemail.php?appName=" + 
        		getResources().getString(R.string.app_id) + "&email=" + email);
        			URLConnection urlConnection = url.openConnection();
        			InputStreamReader isr = new InputStreamReader(
        					urlConnection.getInputStream());
        	        //read in the data from input stream, this can be done a variety of ways
        	        BufferedReader reader = new BufferedReader(isr);
        	        String line= "";
        	        if ((line = reader.readLine()) != null) {
        	            if(line.equals("1")) {
        	            	activity.runOnUiThread(new Runnable() {
        	            	    public void run() {
        	            	    	RayLayout rayLayout = (RayLayout) menu.getChildAt(0);
                	            	ImageView imageView = (ImageView) rayLayout.getChildAt(3);
        	            	        imageView.setImageResource(R.drawable.info_withnew);
        	            	    }
        	            	});
        	            }
        	        }
        		} catch (Exception e) {
        			System.out.println(e.toString());
        		}
                
            }
        }).start();
        
    }
    private void goToAbout() {
    	Intent intent = new Intent(this, AboutActivity.class);
		startActivity(intent);
	}
    
    private void goToFavouriteList() {
    	Intent intent = new Intent(this, ArticleActivity.class);
    	intent.putExtra("parentId", "0");
		startActivity(intent);
	}

    private void goToCategories() {
    	Intent intent = new Intent(this, CategoryActivity.class);
    	intent.removeExtra("parentId");
		startActivity(intent);
	}
    
    private void shareApp() {
		//create the send intent  
		Intent shareIntent =   
		 new Intent(android.content.Intent.ACTION_SEND);  
		  
		//set the type  
		shareIntent.setType("text/plain");  
		  
		//add a subject  
		shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,   
		 "app_name");  
		  
		//build the body of the message to be shared  
		String shareMessage = getResources().getString(R.string.share_app_string);  
		  
		//add the message  
		shareIntent.putExtra(android.content.Intent.EXTRA_TEXT,   
		 shareMessage);  
		  
		//start the chooser for sharing  
		startActivity(Intent.createChooser(shareIntent, getResources().getString(R.string.choose_share_type)));
		launchMarket();
	}

    @Override
    public boolean onPrepareOptionsMenu (Menu menu) {
        return false;
    }

	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_splash_screen, menu);
        return true;
    }
	
	private void launchMarket() {
    	DataService dataService = DataService.getInstance(this);
    	Integer rated = dataService.getRated();
    	if(rated == 2) {
    		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        	alertDialog.setMessage(getResources().getString(R.string.rate_message));
        	alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getResources().getString(R.string.rate_ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                	Uri uri = Uri.parse("market://details?id=" + getPackageName());
        		    Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        		    try {
        		        startActivity(goToMarket);
        		    } catch (ActivityNotFoundException e) {
        		    }
                }
        	});
        	alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getResources().getString(R.string.rate_no), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                	DataService dataService = DataService.getInstance(new SplashScreenActivity());
                	dataService.updateRated(3);
                }
        	});
        	alertDialog.show();
    	}
    	if(rated <= 2) {
    		dataService.updateRated(rated);
    	}
	}
	
	public void onBackPressed() {
        AppBrain.getAds().showInterstitial(this);
        finish();
    }

    @Override
    @TargetApi(value=5)
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (android.os.Build.VERSION.SDK_INT < 5 && keyCode == KeyEvent.KEYCODE_BACK
            && event.getRepeatCount() == 0) {
            // Take care of calling this method on earlier versions of
            // the platform where it doesn't exist.
            onBackPressed();
        }
        return super.onKeyDown(keyCode, event);
    }
}
