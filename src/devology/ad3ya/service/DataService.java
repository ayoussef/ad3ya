package devology.ad3ya.service;

import java.io.IOException;
import java.util.List;

import devology.ad3ya.dao.DataDao;
import devology.ad3ya.model.Article;
import devology.ad3ya.model.Category;

import android.content.Context;

public class DataService {

	private static DataDao dao = null;
	
	private static DataService dataService = null;
	
	public static DataService getInstance(Context context) {
		if(dataService == null) {
			dataService = new DataService(context);
		}
		return dataService;
	}

	public DataService(Context context) {
		init(context);
	}

	private void init(Context context) {
		if(dao == null) {
			dao = new DataDao(context);
		}
	}

	public void addCategory(Category category) {
		dao.addCategory(category);
	}

	public Category getCategory(int id) {
		return dao.getCategory(id);
	}

	public void createDatabase(Boolean testing) throws IOException {
		dao.createDataBase(testing);
	}

	public List<Category> getCategories(Integer parentId) {
		return dao.getCategories(parentId);
	}

	public List<Article> getArticlesByParentId(Integer parentId) {
		return dao.getArticlesByParentId(parentId);
	}

	public void saveArticle(Article article) {
		dao.saveArticle(article);
	}

	public List<Article> getFavouritArticles() {
		return dao.getFavouritArticles();
	}

	public Integer getRated() {
		return dao.getRated();
	}

	public void updateRated(Integer rated) {
		dao.updateRated(rated + 1);
	}
	
}
